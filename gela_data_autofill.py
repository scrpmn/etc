""" Gela was kinda special vendor… """
import requests
from bs4 import BeautifulSoup


ART = 'PMA803120'
SEARCH = 'http://www.gela.ru/s/'
PARSER = 'html5lib'
PARAMS = {'q': ART, 'w': 'article', 'how': 'r'}
soup = BeautifulSoup(requests.get(SEARCH, params=PARAMS).content, PARSER)  # pylint: disable=C0103
items = soup.select('.page-catalog-subsection .border_top')  # pylint: disable=C0103
for item in items:
    item_art = item.select('.preview-info-footer')[0].get_text().strip()[4:30].strip()
    if item_art == ART:
        data = {x[0]: x[1] for x in map(lambda s: s.split(': ', 1), [i.get_text().strip().replace('  ', ' ') for i in
                                                                     item.select('.product-property')])}
