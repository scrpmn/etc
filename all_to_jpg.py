""" convert all the images in the folder to jpg """
import os
import shutil
import sys

from PIL import Image


QUALITY = 97
OPTIMIZE = True
PROGRESSIVE = True
SUBSAMPLING = 0

folder = sys.argv[1]  # pylint: disable=C0103
save_folder = os.path.join(folder, os.path.basename(folder))  # pylint: disable=C0103

if not os.path.exists(save_folder):
    os.mkdir(save_folder)
for file in os.listdir(folder):
    if file.endswith('.gif' or '.png' or '.bmp'):
        path = os.path.join(folder, file)
        img = Image.open(path)
        img.save(os.path.join(save_folder, file[:-4] + '.jpg'),
                 quality=QUALITY,
                 optimize=OPTIMIZE,
                 progressive=PROGRESSIVE,
                 subsampling=SUBSAMPLING)
    elif file.endswith('.jpg'):
        shutil.copyfile(os.path.join(folder, file), os.path.join(save_folder, file))
