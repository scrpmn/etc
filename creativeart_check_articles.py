""" For vendor Creative Art """
import os
import urllib.request
from time import sleep

from bs4 import BeautifulSoup

import util


FOLDER = 'C:\\py\\scrpmn_save_photo'
INTERVAL = 0.05
FILE = 'КреативАрт 516.xlsx'
CYR = 'http://креатив-артшопп.рф'
LAT = 'http://xn----7sbbgntl2bjacgvf4g.xn--p1ai'

path = os.path.join(FOLDER, FILE)  # pylint: disable=C0103
sheet = util.load(path)  # pylint: disable=C0103
wb = sheet[sheet.get_sheet_names()[0]]  # pylint: disable=C0103
e = 1  # pylint: disable=C0103
while wb['A' + str(e)].value is not None:
    e += 1
for i in range(1, e):
    soup = BeautifulSoup(urllib.request.urlopen(wb['E' + str(i)].value.replace(CYR, LAT)).read(), 'html.parser')
    wb['D' + str(i)].value = soup.body.find('div', {'class': 'product-code'}).contents[1].string.replace(' ', '')
    if wb['D' + str(i)].value != wb['C' + str(i)].value:
        wb['F' + str(i)].value = '!'
    print('Article {} is done.'.format(wb['A' + str(i)].value))
    sleep(INTERVAL)
util.write(sheet, path[:-5] + '(1).xlsx')
