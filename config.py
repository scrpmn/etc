""" Base config reader from config.ini """

import configparser
import os
import sys

FILENAME = 'config.ini'
file = os.path.join(os.path.dirname(sys.argv[0]), FILENAME)  # pylint: disable=C0103
config = configparser.RawConfigParser()  # pylint: disable=C0103
config.read(file)
TEST_GROUP = int(config.get('Groups', 'Test'))
PRODUCTION_GROUP = int(config.get('Groups', 'Production'))
TOKENS = [i[1] for i in config.items('Tokens')]
