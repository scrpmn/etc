""" When it was possible to get audio with VK API """
import os
from time import sleep
from html import unescape as un
from urllib.request import urlretrieve

import vk

from config import TOKENS


PATH = 'C:\\py\\scrpnm_copy_album_vk\\!vk\\'
INTERVAL = 0.35  # tried different ones
ID = 1  # for example

if not os.path.exists(PATH):
    os.mkdir(PATH)
vk_api = vk.API(vk.Session(access_token=TOKENS[0]))  # вместо TOKEN должен быть ваш токен  pylint: disable=C0103
audio = vk_api.audio.get(owner_id=ID)  # pylint: disable=C0103
for t in range(1, len(audio)):
    filename = '{}{} - {}.mp3'.format(PATH, un(audio[t]['artist']), un(audio[t]['title'])).replace('|', '')
    if os.path.exists(filename):
        pass
    else:
        print(filename)
        urlretrieve(audio[t]['url'], filename)
        sleep(INTERVAL)
