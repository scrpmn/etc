"""" Functions for photo resizing """
import os
import sys
from collections import OrderedDict
from urllib.request import urlretrieve
from urllib.error import URLError
from time import sleep

from openpyxl import load_workbook


def get_dict(file):
    """ Match photo and article """
    data = OrderedDict()
    sheet = load_workbook(file)
    wb = sheet[sheet.get_sheet_names()[0]]  # pylint: disable=C0103
    n = 1  # pylint: disable=C0103
    while value(wb, 'A', n) is not None:
        photo_id = value(wb, 'B', n)
        article = value(wb, 'A', n)
        data.update({article: photo_id})
        n += 1  # pylint: disable=C0103
    return data


def value(sheet, column, line):
    """ Get value of the cell """
    return sheet[column + str(line)].value


def save_img(data, file, pause=0.05):
    """ Save image """
    if not os.path.exists(file):
        os.mkdir(file)
    for key, img_link in data.items():
        try:
            urlretrieve(img_link, os.path.join(file, str(key) + '.jpg'))
        except URLError:
            print(f'Не удаётся сохранить изображение по артикулу {key}.')
            print(img_link)
        sleep(pause)


def get_file():
    """ Get file """
    return sys.argv[1:][0]
