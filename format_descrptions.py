""" Format descriptions """
import os

import util

FOLDER = 'C:\\РАБОЧАЯ'
FILE = 'СкрапКрым 425 Накладная af.xlsx'
FIND_END_ROW = 'a'

path = os.path.join(FOLDER, FILE)  # pylint: disable=C0103
sheet = util.load(path)  # pylint: disable=C0103
wb = sheet[sheet.get_sheet_names()[0]]  # pylint: disable=C0103
end = 2  # pylint: disable=C0103
while wb[FIND_END_ROW + str(end)].value is not None:
    end += 1
for i in range(2, end):
    cell = str(wb['H' + str(i)].value)
    if cell != '_':
        if not cell.endswith('см'):
            cell += ' см'
        cell = cell.replace('см', ' см')\
                   .replace('  ', ' ')\
                   .replace('*', 'х')\
                   .replace('.', ',')
    if wb['F' + str(i)].value is None:
        cell = 'Размер ' + cell
    wb['H' + str(i)].value = cell
util.write(sheet, path[:-5] + ' af.xlsx')
