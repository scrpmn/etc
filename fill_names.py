""" Fill names """
import os

import util


FOLDER = 'C:\\РАБОЧАЯ'
FILE = 'СкрапКрым 425 Накладная af af.xlsx'
NAME_PARTS = {'м': 'Набор украшений из чипборда',
              'над': 'Украшение из чипборда-надпись',
              'я': 'Украшение из чипборда'}

path = os.path.join(FOLDER, FILE)  # pylint: disable=C0103
sheet = util.load(path)  # pylint: disable=C0103
wb = sheet[sheet.get_sheet_names()[0]]  # pylint: disable=C0103
end = util.find_end(wb)  # pylint: disable=C0103
for i in range(2, end):
    t = wb['F' + str(i)].value
    if t in NAME_PARTS:
        wb['B' + str(i)].value = '{type} "{name}"'\
            .format(type=NAME_PARTS[t],
                    name=wb['B' + str(i)].value.rstrip(' '))
util.write(sheet, path[:-5] + '(1).xlsx')
