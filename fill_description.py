"""" Fill description """
import urllib.request
import os
from time import sleep

from bs4 import BeautifulSoup

import util


FOLDER = 'C:\\РАБОЧАЯ'
FILE = 'СкрапКрым 425 Накладная.xlsx'
FIND_END_ROW = 'A'

path = os.path.join(FOLDER, FILE)  # pylint: disable=C0103
sheet = util.load(path)  # pylint: disable=C0103
wb = sheet[sheet.get_sheet_names()[0]]  # pylint: disable=C0103
end = 2  # pylint: disable=C0103
while wb[FIND_END_ROW + str(end)].value is not None:
    end += 1
for i in range(2, end):
    description = wb['H' + str(i)].value
    if description is None:
        link = wb[FIND_END_ROW + str(i)].value
        soup = BeautifulSoup(urllib.request.urlopen(link).read(), 'html.parser')
        text = soup.body.find('div', {'id': 'main-wrapper'}) \
            .find('div', {'id': 'wrapper'}) \
            .find('div', {'id': 'container'}) \
            .find('section', {'id': 'content'}) \
            .find('section', {'data_list-widget-id': '9'}) \
            .find('section', {'id': 'products'}) \
            .find('div', {'id': 'products-show'}) \
            .find('div') \
            .find('article', {'class': 'item-content res-wrapper'}) \
            .find('div', {'class': 'product-content'}) \
            .find('div', {'class': 'clearfix'}) \
            .find('div', {'class': 'user-inner'}).p
        try:
            text.string.replace('\n', ' ')
            wb['H' + str(i)].value = text.string
        except AttributeError:
            wb['H' + str(i)].value = '_'
        sleep(0.05)
        print(i)
util.write(sheet, path[:-5] + ' af.xlsx')
