""" literally unstrong html imported file """
import sys

INPUT_FILE = sys.argv[1:][0]
OUTPUT_FILE = INPUT_FILE[:-4] + ' clear.txt'
with open(INPUT_FILE, 'r', encoding='utf-8') as f:
    TEXT = f.read()
TEXT = TEXT.replace('<strong><strong>', '<strong>')\
           .replace('</strong></strong>', '</strong>')\
           .replace('<strong></strong>', '')\
           .replace('</strong><strong>', '')
with open(OUTPUT_FILE, 'w', encoding='utf-8') as f:
    f.write(TEXT)
