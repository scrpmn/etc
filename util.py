""" Procedures used in other modules """
from copy import deepcopy as copy

from openpyxl import load_workbook

import config as c


def load(file):
    """ Load workbook """
    return load_workbook(file)


def write(sheet, file):
    """ Write into workbook """
    sheet.save(file)


def find_end(workbook):
    """ Find the last filled line in sheet """
    e = 1  # pylint: disable=C0103
    while workbook['d' + str(e)].value is not None:
        e += 1  # pylint: disable=C0103
    return e


def show_on_site(workbook, cell):
    """ Do we show this article on our site? """
    workbook[c.show + str(cell)].value = 'Да'  # pylint: disable=E1101


def copy_description(workbook, cell):
    """ Copy description of article """
    workbook[c.full_descr + str(cell)].value = copy(workbook[c.short_descr + str(cell)].value)  # pylint: disable=E1101


def format_cell(s):  # pylint: disable=C0103
    """ Remove unwanted space symbols """
    if s is None:
        s = ''
    return str(s).lstrip(' ').rstrip(' ')


def format_name(s):  # pylint: disable=C0103
    """ Some clean-up for contains of cell """
    return s.replace(' ()', '')\
            .replace('(, ', '(')\
            .replace(', )', ')')\
            .replace('  ', ' ')


def collect_names(workbook, cell, show_brand=True, scb=False):
    """ Create full and short names for site and IC """
    if workbook[c.our_article + str(cell)].value is None:  # pylint: disable=E1101
        name_full = workbook[c.full_name + str(cell)].value  # pylint: disable=E1101
        name_short = workbook[c.short_name + str(cell)].value  # pylint: disable=E1101
        article = format_cell(workbook[c.article + str(cell)].value)  # pylint: disable=E1101
        brand = format_cell(workbook[c.brand + str(cell)].value)  # pylint: disable=E1101
        if brand in c.empty_brands:  # pylint: disable=E1101
            brand = ''
        if show_brand is not True:
            name_short = copy(name_full)
        else:
            name_short = f'{name_full} ({brand})'
        if scb is False:
            name_full = f'{name_full} ({brand}, {article})'
        else:
            name_full = f'{name_full} ({article.replace("SCB", "SCB, ")})'

        workbook[c.full_name + str(cell)].value = format_name(name_full)  # pylint: disable=E1101
        workbook[c.short_name + str(cell)].value = format_name(name_short)  # pylint: disable=E1101
    else:
        workbook[c.full_name + str(cell)].value = None  # pylint: disable=E1101
        for row in workbook[c.group + str(cell) : c.base2 + str(cell)]:  # pylint: disable=E1101
            for row_cell in row:
                row_cell.value = None


def null_cell(workbook, cell, line):
    """ Make cell null """
    workbook[line + str(cell)].value = None
