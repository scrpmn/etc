""" Fast converting invoices from USA to continue parsing it manually """
import re
import argparse

import util


ART_ARRAY = [27, 12, 14, 20]


def remove_article_beginning(cell, data):
    """ sometimes there was junk in the article name """
    for s in data:  # pylint: disable=C0103
        if cell.startswith(str(s)):
            return cell.replace(str(s), '', 1)
    return cell


parser = argparse.ArgumentParser(description='Конвертируем счета США в Excel')  # pylint: disable=C0103
parser.add_argument('path', metavar='P', type=str, help='Путь к таблице')
path = parser.parse_args().path  # pylint: disable=C0103

sheet = util.load(path)  # pylint: disable=C0103
wb = sheet[sheet.get_sheet_names()[0]]  # pylint: disable=C0103
wb['A1'].value = 'АРТИКУЛ'
wb['B1'].value = 'НАЗВАНИЕ'
wb['C1'].value = 'РРЦ'
wb['D1'].value = 'КОД'
wb['E1'].value = 'К-ВО'
wb['F1'].value = 'ЦЕНА'
wb['G1'].value = 'СУММА'
i = 2
while wb['A' + str(i)].value is not None:
    new_art_array = [str(s) + ' ' for s in ART_ARRAY]  # pylint: disable=C0103
    wb['A' + str(i)].value = remove_article_beginning(wb['A' + str(i)].value, new_art_array)
    wb['A' + str(i)].value = re.sub(' c$', '', str(wb['A' + str(i)].value))
    data_list = wb['A' + str(i)].value.split(' ')  # pylint: disable=C0103
    wb['G' + str(i)].value = float(data_list[-1])
    data_list.pop(-1)
    wb['F' + str(i)].value = float(data_list[-1])
    data_list.pop(-1)
    wb['E' + str(i)].value = int(data_list[-1].replace('.00', ''))
    data_list.pop(-1)
    wb['D' + str(i)].value = data_list[-1]
    data_list.pop(-1)
    wb['C' + str(i)].value = float(data_list[-1])
    data_list.pop(-1)
    try:
        data_list[0] = int(data_list[0])
    except ValueError:
        pass
    if type(data_list[0]) is str:
        data_list[0] = remove_article_beginning(data_list[0], ART_ARRAY)
    wb['A' + str(i)].value = data_list[0]
    data_list.pop(0)
    wb['B' + str(i)].value = ' '.join(data_list)
    i += 1
util.write(sheet, path[:-5] + '(1).xlsx')
