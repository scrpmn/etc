"""" Auto fill our tables with data from SimaLand site """
import sys
import math
import urllib.request

from bs4 import BeautifulSoup

import util


path = sys.argv[1:][0]  # pylint: disable=C0103
sheet = util.load(path)  # pylint: disable=C0103
wb = sheet[sheet.get_sheet_names()[0]]  # pylint: disable=C0103
end = util.find_end(wb)  # pylint: disable=C0103
for i in range(2, end):
    if wb['b' + str(i)].value is not None:
        article = wb['o' + str(i)].value
        link = 'https://www.sima-land.ru/{}/'.format(article)
        wb['f' + str(i)].hyperlink = link
        wb['s' + str(i)].hyperlink = link
        soup = BeautifulSoup(urllib.request.urlopen(link).read(), 'html.parser')
        description = soup.body.find('ul', {'class': 'g-ul b-properties'}) \
            .find_all('li', {'class': 'b-properties__item'})
        for e in description:
            if 'Вес' in e.find('span', {'class': 'b-properties__label'}).get_text():
                weight = e.find('span', {'class': 'b-properties__value'}).get_text().replace(' г', '')
                try:
                    weight = int(weight)
                except ValueError:
                    weight = math.ceil(float(weight))  # TODO: all into float
                wb['k' + str(i)].value = weight + 2
        wb['b' + str(i)].value = wb['b' + str(i)].value\
            .replace('*', 'x').replace(' x ', 'x').replace('см', ' см').replace('  ', ' ')
        parts = wb['b' + str(i)].value.split(' ')
        if parts[-1] == str(article):
            parts = parts.pop(-1)
        if parts[-1] == 'см':
            wb['h' + str(i)].value = ' '.join(parts[-2:])
            wb['b' + str(i)].value = ' '.join(parts[:-2]).rstrip(',')
util.write(sheet, path[:-5] + '(1).xlsx')
