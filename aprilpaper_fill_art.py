""" Parse some info from vendor named AprilPaper """
import os
import urllib.request

from bs4 import BeautifulSoup

import util


FOLDER = 'C:\\РАБОЧАЯ'
INTERVAL = 0.05
FILE = 'April Paper.xlsx'

path = os.path.join(FOLDER, FILE)  # pylint: disable=C0103
sheet = util.load(path)  # pylint: disable=C0103
wb = sheet[sheet.get_sheet_names()[0]]  # pylint: disable=C0103
for i in range(2, util.find_end(wb)):
    if wb['A' + str(i)].value is None:
        link = wb['F' + str(i)].value
        soup = BeautifulSoup(urllib.request.urlopen(link).read(), 'html.parser')
        wb['P' + str(i)].value = soup.body.find('div', {'class': 'shop2-product-article'}) \
            .get_text().replace('Артикул: ', '')
util.write(sheet, path[:-5] + '(1).xlsx')
