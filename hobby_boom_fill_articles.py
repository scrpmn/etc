""" for vendor Hobby Boom """
import sys

import util


path = sys.argv[1:][0]  # pylint: disable=C0103
sheet = util.load(path)  # pylint: disable=C0103
wb = sheet[sheet.get_sheet_names()[0]]  # pylint: disable=C0103
end = util.find_end(wb)  # pylint: disable=C0103
for i in range(2, end):
    art = wb['O' + str(i)].value
    wb['S' + str(i)].value = art
    wb['S' + str(i)].hyperlink = 'http://www.hobby-opt.ru/files/originals/{}.jpg'.format(art)
util.write(sheet, path[:-5] + '(1).xlsx')
