""" For invoices from vendor 7Dots """
import itertools
import string

import util


ROWS = 9


def convert_to_int(s):  # pylint: disable=C0103
    """ Convert article to integer """
    s = s.rstrip().lstrip()
    try:
        return int(s.replace(',00', ''))
    except ValueError:
        try:
            return float(s.replace(',', '.'))
        except ValueError:
            return s


PATH = 'C:\\РАБОЧАЯ\\7dot_invoice.xlsx'
sheet = util.load(PATH)  # pylint: disable=C0103
wb = sheet[sheet.get_sheet_names()[0]]  # pylint: disable=C0103
wb['A1'].value = 'No.'
wb['B1'].value = 'Product name'
wb['C1'].value = 'Unit'
wb['D1'].value = 'Quantity'
wb['E1'].value = 'Unit Net price'
wb['F1'].value = 'Discount'
wb['G1'].value = 'Total Net price'
wb['H1'].value = 'VAT'
wb['I1'].value = 'Gross'

data = 2  # pylint: disable=C0103
while wb['A' + str(data)].value is not None:
    material = [convert_to_int(x) for x in wb['A' + str(data)].value.split('|')][1:]  # pylint: disable=C0103
    for idx, row in enumerate(itertools.chain(string.ascii_uppercase[:ROWS])):
        wb[row + str(data)].value = material[idx]
    data += 1
util.write(sheet, 'C:\\РАБОЧАЯ\\7dot_invoice Ready.xlsx')
